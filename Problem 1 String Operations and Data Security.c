#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#define SIZE 97

void PrintCharArray(const char *);
void EncodeMessage(char *, const int *, const int, const int);
void DecodeMessage(char *, const int *, const int, const int);
int MessageSize(const char *);

int main (){
	int PrimeNum[]={1,2,3,5,7,11,13,17,19,23,29,31};
	//initialized an integer array of prime numbers
	int PrimeNumElements = (sizeof PrimeNum)/4;
	//initialized an integer equiavelent to the number of elements of PrimeNum
	
	char message[SIZE] = {0};
	//initializes a character array of size SIZE, and sets all elements to 0
	puts("Enter a message to be encoded (max 96 characters): \n");
	fgets(message, SIZE, stdin);
	
	puts ("\nThe encoded message is as follows:\n");
	EncodeMessage(message, PrimeNum, MessageSize(message), PrimeNumElements);
	//message is encoded
	PrintCharArray(message);

	puts ("The original message is as follows: \n");
	DecodeMessage(message, PrimeNum, MessageSize(message), PrimeNumElements);
	//message is decoded
	PrintCharArray(message);
	
	return 0;
}

void PrintCharArray(const char *caPtr){
	size_t i;
	for(i = 0; caPtr[i] != '\0'; i++){
		printf ("%c", caPtr[i]);
	}//This for statement prints out each array element, not including '\0'
	puts ("\n");
}//This function will simply print character arrays

void EncodeMessage(char *caPtr, const int *naPtr, const int caSize, const int naSize){
	size_t i = 0;
	size_t j;
	while (i < caSize){
		for (j = 0; i < (caSize) && caPtr[i] != '\0' && j != naSize; i++, j++){
			caPtr[i] += naPtr[j];
		}//The element caPtr[i] will be set to caPtr[i]+naPtr[j]
		//after each loop i & j are incremented, and the loop will only continue
		//if i< caSize, and jdoes not equal naSize, and the current element of 
		//caPtr[i] is not '\0'
		//On each repetition of the while loop, i will not reset, but j will reset to 0
		for (j = naSize - 1; i < (caSize) && caPtr[i] != '\0' && j != -1; i++, j--){
			if (caPtr[i] == 32){
				caPtr[i] = (caPtr[i] + 94) - naPtr[j];
				//^Treats spaces like the character "~"
				//Without this conversion, a space may be encoded into a 
				//null character (space ascii = 32. if naPtr[j] = 31, then 
				//caPtr[i] = 1, making the space a '\0' and therefore terminate
				//the printing of the string)
			}else{
				caPtr[i] -= naPtr[j];
			}
		}//The element caPtr[i] will be set to caPtr[i]-naPtr[j]
		//after each loop i & j are incremented, and the loop will only continue
		//if i< caSize, and jdoes not equal a negative number, and the current element of 
		//caPtr[i] is not '\0'
		//On each repetition of the while loop, i will not reset, but j will reset to naSize -1
		//This ensures that the two for loops will continually modify caPtr[i] in groups
		//of 12. (if the message size is less than a multiple of 12, the for loop will terminate)
		
	}//At the end of the two for loops, i does not reset to 0, so the while statement
	//will loop again as long as i < caSize
}

void DecodeMessage(char *caPtr, const int *naPtr, const int caSize, const int naSize){
	size_t i = 0;
	size_t j;
	while (i < caSize){
		for (j = 0; i < (caSize) && caPtr[i] != '\0' && j != naSize; i++, j++){
			caPtr[i] -= naPtr[j];
		}
		//Same flow as EncodeMessage, except caPtr[i] = caPtr[i] - naPtr[j]
		for (j = naSize - 1; i < (caSize) && caPtr[i] != '\0' && j != -1; i++, j--){
			caPtr[i] += naPtr[j];
			if (caPtr[i] == 126){
				caPtr[i] -= 94;
			} //This if statement converts "~" to spaces
			//after decoding, if there were spaces in the original message,
			//they would be printed as "~" because of the conversion in EncodeMessage
			//therefor the if() statement above converts "~" into spaces.
		}//Same flow as Encode message, except caPtr[i] = caPtr[i] + naPtr[j]
		
	}
}

int MessageSize(const char *caPtr){
	size_t i;
	int elements = -1;
	for (i = 0; caPtr[i] != '\0'; i++){
		if (caPtr[i] != 0){
			elements += 1;
		}//For every character that is not '\0', elements is incremented by 1
	}
	return (elements);
}//This function returns the number of characters inputed to a
//character array, not including '\0'