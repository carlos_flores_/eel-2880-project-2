# EEL 2880 Project 2: Data Security Implementations

## Objectives of this project:

* To implemenet dynamic character coding
	* The encoded message must consist in quantities of 12 characters.
	* It must also contain a minimum of 72 characters.
  

* Decode the message to the original.

## Methodology:

1. Convert each character of the message to upper case.

2. Modify the first 12 characters of the original message using the first 12 prime numbers: [1,2,3,5,7,11,13,17,19,23,29,31]
  
	* Incremenet each respective original message character with a matching prime number.

3. Modify the next 12 using the reverse order of the first 12 prime numbers.

	* Decrement each respective original message character with a matching prime number.

4. Repeat 2 and 3, in order, until the message has been fully encoded.

5. Decode the message.

###### Current Project Version: V2.3
